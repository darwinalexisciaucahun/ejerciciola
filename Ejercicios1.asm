;DarwinAlexisCiauCahun
;Ejercicio1

.model small


.stack 64
.data
  
;declarando variables globales
numero1 db 0
numero2 db 0

suma db 0
resta db 0
multiplicacion db 0
division db 0

mensaje1 db 10,13, "Ingrese el primer numero a realizar: ",'$';ingrese n1
mensaje2 db 10,13, "Ingrese el segundo numero a realizar: ",'$';ingrese n2

mensajeS db 10,13, "La suma es= ",'$'
mensajeR db 10,13, "La resta= ",'$'
mensajeM db 10,13, "La Multiplicacion es= ",'$'
mensajeD db 10,13, "La division es = ",'$'  

.code
begin proc far
    
    ;direccionamiento del procedimiento
    mov ax, @data
    mov ds,ax
    
    ;solicitar numero 1
    
    mov ah, 09
    lea dx, mensaje1
    int 21h
    mov ah, 01
    int 21h
    sub al, 30h
    mov numero1,al
    
    ;solicitar numero 2
    
    mov ah, 09
    lea dx, mensaje2
    int 21h
    mov ah, 01
    int 21h
    sub al, 30h
    mov numero2,al
    
    ;operaciones
                  
    ;SUMA             
    mov al,numero1
    add al,numero2
    mov suma,al  
    
    ;RESTA
    mov al,numero1
    sub al,numero2
    mov resta,al
    
    ;MULTIPLICACION
    mov al,numero1
    mul numero2
    mov multiplicacion,al
    
    ;DIVISION
    mov al,numero1
    div numero2
    mov division,al
       
    ;Resultados (Algunos resultados en codigo ASCII) 
    
    ;suma
    mov ah,09
    lea dx,mensajeS
    int 21h
    mov dl,suma
    add dl,30h 
    mov ah,02
    int 21h  
    
    ;resta
    mov ah,09
    lea dx,mensajeR
    int 21h
    mov dl,resta
    add dl,30h 
    mov ah,02
    int 21h
    
    ;multiplicacion
    mov ah,09
    lea dx,mensajeM
    int 21h
    mov dl,multiplicacion
    add dl,30h 
    mov ah,02
    int 21h
    
    ;division
    mov ah,09
    lea dx,mensajeD
    int 21h
    mov dl,division
    add dl,30h 
    mov ah,02
    int 21h
                
    mov ah,4ch
    int 21h
    
    begin endp
end